module.exports = async (...args) => {
    const lastArgument = args[args.length - 1];
    const penultArgument = args[args.length - 2];

    if (typeof lastArgument !== 'function' && typeof penultArgument !== 'function') {
        throw TypeError('There have to be a reducer function in arguments.');
    }

    let reducer, input, initialValue;

    if (typeof lastArgument === 'function') {
        reducer = lastArgument;
        input = args.slice(0, args.length - 1);
    } else if (typeof lastArgument === 'number' && typeof penultArgument === 'function') {
        reducer = penultArgument;
        initialValue = lastArgument;
        input = args.slice(0, args.length - 2);
    }

    let accumulator;
    let currentIndex = 0;

    if (!input.length && initialValue === undefined) {
        throw TypeError('Reduce of empty array with no initial value.');
    }

    if (!input.length && initialValue !== undefined) {
        return initialValue;
    }

    for await (const currentValue of input) {
        if (currentIndex === 0 && initialValue === undefined) {
            accumulator = currentValue;
            currentIndex++;
            continue;
        } else if (currentIndex === 0 && initialValue !== undefined) {
            accumulator = initialValue;
            currentIndex++;
        }

        accumulator = reducer(accumulator, currentValue, currentIndex, input);
    }

    return accumulator;
}
