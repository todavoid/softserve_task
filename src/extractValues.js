module.exports = (dataOrig, logger) => {
	function go(data, traversedVertexes) {
		if (traversedVertexes.has(data)) return;
		if (typeof data !== 'object' || typeof data === 'function' || data === null) return;
		if (data instanceof WeakMap) return;
		if (data instanceof WeakSet) return;

		traversedVertexes.add(data);
		const values = data instanceof Set || data instanceof Map
			? data.values()
			: Object.values(data);

		for (let value of values) {
			if (typeof value === 'object' && value !== null) {
				go(value, traversedVertexes);
			} else {
				logger(value);
			}
		}
	}

	go(dataOrig, new Set());
};
