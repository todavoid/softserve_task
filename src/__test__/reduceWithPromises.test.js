const reduce = require('../reduceWithPromises');

describe('reduceWithPromises', () => {
    it('should throw TypeError if called with no arguments.', async () => {
        const ERROR_MESSAGE = 'There have to be a reducer function in arguments.';

        await expect(reduce())
            .rejects
            .toThrow(ERROR_MESSAGE);
    });

    it('should throw TypeError if called with no reducer function.', async () => {
        const ERROR_MESSAGE = 'There have to be a reducer function in arguments.';

        await expect(reduce(1, 2))
            .rejects
            .toThrow(ERROR_MESSAGE);
    });

    it('should throw TypeError if called with no arguments and no initial value.', async () => {
        const mockReducer = jest.fn();
        const ERROR_MESSAGE = 'Reduce of empty array with no initial value.';

        await expect(reduce(mockReducer))
            .rejects
            .toThrow(ERROR_MESSAGE);
    });

    describe('if no initial value', () => {
        it('should run reducer values.length -1 times ', async () => {
            const values = [1, 2, 3];
            const mockReducer = jest.fn();
            const expected = values.length - 1;

            await reduce(...values, mockReducer);

            expect(mockReducer).toHaveBeenCalledTimes(expected);
        });

        it('should run reducer with values[0] as accumulator', async () => {
            const values = [1, 2, 3];
            const mockReducer = jest.fn();
            const expected = values[0];

            await reduce(...values, mockReducer);

            expect(mockReducer.mock.calls[0][0]).toBe(expected);
        });

        it('should run reducer with values[1] as currenValue ', async () => {
            const values = [1, 2, 3];
            const mockReducer = jest.fn();
            const expected = values[1];

            await reduce(...values, mockReducer);

            expect(mockReducer.mock.calls[0][1]).toBe(expected);
        });

        it('if value is a single element, should return that element without calling reducer.', async () => {
            const value = 1;
            const mockReducer = jest.fn();
            const expected = value;

            const actual = await reduce(value, mockReducer);

            expect(mockReducer).not.toHaveBeenCalled();
            expect(actual).toBe(expected);
        });
    });

    describe('if initial value', () => {
        it('should return initialValue without calling reducer if no values passed', async () => {
            const mockReducer = jest.fn();
            const initialValue = 0;
            const expected = initialValue;

            const actual = await reduce(mockReducer, initialValue);

            expect(mockReducer).not.toHaveBeenCalled();
            expect(actual).toBe(expected);
        });

        it('should run reducer values.length times. ', async () => {
            const values = [1, 2, 3];
            const mockReducer = jest.fn();
            const initialValue = 0;
            const expected = values.length;

            await reduce(...values, mockReducer, initialValue);

            expect(mockReducer).toHaveBeenCalledTimes(expected);
        });

        it('should run reducer with initialValue as accumulator.', async () => {
            const values = [1, 2, 3];
            const mockReducer = jest.fn();
            const initialValue = 0;
            const expected = initialValue;

            await reduce(...values, mockReducer, initialValue);

            expect(mockReducer.mock.calls[0][0]).toBe(expected);
        });

        it('should run reducer with values[0] as currentValue.', async () => {
            const values = [1, 2, 3];
            const mockReducer = jest.fn();
            const initialValue = 0;
            const expected = values[0];

            await reduce(...values, mockReducer, initialValue);

            expect(mockReducer.mock.calls[0][1]).toBe(expected);
        });
    });

    it('should actually reduce', async () => {
        const values = [1, 2, 3];
        const reducer = (memo, next) => memo + next;
        const expected = 6;

        const actual = await reduce(...values, reducer);

        expect(actual).toBe(expected);
    });

    it('should actually reduce with promises', async () => {
        const promise = new Promise((resolve) => setTimeout(() => resolve(1), 0));
        const values = [promise, 2, 3];
        const reducer = (memo, next) => memo + next;
        const expected = 6;

        const actual = await reduce(...values, reducer);

        expect(actual).toBe(expected);
    });
});
