const extract = require('../extractValues');

describe('extractValues', () => {
    it('should skip WeakMap values.', async () => {
        const weakMap = new WeakMap();
        weakMap.set({ prop: 0 }, 0);
        const mockLogger = jest.fn();

        extract(weakMap, mockLogger);

        expect(mockLogger).not.toHaveBeenCalled();
    });

    it('should skip WeakSet values.', async () => {
        const weakSet = new WeakSet([{ prop: 0 }]);
        const mockLogger = jest.fn();

        extract(weakSet, mockLogger);

        expect(mockLogger).not.toHaveBeenCalled();
    });

    it('should traverse trough array and log all the values.', async () => {
        const arr = [1, 2];
        const expected = [1, 2];
        const mockLogger = jest.fn();

        extract(arr, mockLogger);

        expect(mockLogger.mock.calls.flat()).toEqual(expected);
    });

    it('should traverse trough object and log all the values.', async () => {
        const obj = { prop1: 1, prop2: 2 };
        const expected = [1, 2];
        const mockLogger = jest.fn();

        extract(obj, mockLogger);

        expect(mockLogger.mock.calls.flat()).toEqual(expected);
    });

    it('should traverse trough Map and log all the values.', async () => {
        const map = new Map();
        map.set('prop1', 1);
        map.set('prop2', 2);
        const expected = [1, 2];
        const mockLogger = jest.fn();

        extract(map, mockLogger);

        expect(mockLogger.mock.calls.flat()).toEqual(expected);
    });

    it('should traverse trough Set and log all the values.', async () => {
        const set = new Set([1, 2]);
        const expected = [1, 2];
        const mockLogger = jest.fn();

        extract(set, mockLogger);

        expect(mockLogger.mock.calls.flat()).toEqual(expected);
    });

    it('should traverse trough nested array and log all the values.', async () => {
        const arrayNested = [1, [2, [3]]];
        const expected = [1, 2, 3];
        const mockLogger = jest.fn();

        extract(arrayNested, mockLogger);

        expect(mockLogger.mock.calls.flat()).toEqual(expected);
    });

    it('should traverse trough nested object and log all the values.', async () => {
        const objectNested = { prop1: { prop2: 1, prop3: { prop4: 2 } } };
        const expected = [1, 2];
        const mockLogger = jest.fn();

        extract(objectNested, mockLogger);

        expect(mockLogger.mock.calls.flat()).toEqual(expected);
    });

    it('should prevent stack overflow when facing acyclic dependencies.', async () => {
        const acyclicDepObject1 = { b: 1 };
        const acyclicDepObject2 = { a: acyclicDepObject1 };
        acyclicDepObject1.c = acyclicDepObject2;
        const expected = 1;
        const mockLogger = jest.fn();

        extract(acyclicDepObject1, mockLogger);

        expect(mockLogger).toHaveBeenCalledWith(expected);
    });
});
